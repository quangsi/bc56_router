import React from "react";
import { TAI } from "./utils";

export default function KetQua({
  handlePlayGame,
  luaChon,
  soLanThang,
  soLuotChoi,
}) {
  return (
    <div className="pt-5">
      <button onClick={handlePlayGame} className="btn btn-warning px-5 py-2">
        Play game
      </button>
      {/* nếu luaChon có giá trị thì hiện thẻ h2  */}
      {luaChon && (
        <h2 className={luaChon == TAI ? "text-danger" : "text-dark"}>
          Lựa chọn: {luaChon}
        </h2>
      )}
      <h2>Số lần thắng: {soLanThang}</h2>
      <h2>Số lượt chơi: {soLuotChoi}</h2>
    </div>
  );
}
