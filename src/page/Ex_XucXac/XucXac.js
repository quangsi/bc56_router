import React from "react";
import { TAI, XIU } from "./utils";

export default function XucXac({ mangXucXac, setLuaChon }) {
  return (
    <div className="d-flex align-items-center justify-content-center">
      <button
        onClick={() => {
          setLuaChon(TAI);
        }}
        className="p-5 btn btn-danger"
      >
        Tài
      </button>
      <div className="mx-5">
        {mangXucXac.map((item) => {
          return <img width={80} src={item.img} alt="" />;
        })}
      </div>
      <button
        onClick={() => {
          setLuaChon(XIU);
        }}
        className="p-5 btn btn-dark"
      >
        Xỉu
      </button>
    </div>
  );
}
