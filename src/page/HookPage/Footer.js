import React from "react";

export default function Footer({ number, handleReset }) {
  return (
    <div>
      <h1>{number * 2}</h1>
      <button onClick={handleReset} className="btn btn-info">
        Reset
      </button>
    </div>
  );
}
