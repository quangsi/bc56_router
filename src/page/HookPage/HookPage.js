import React, { useState } from "react";
import Footer from "./Footer";

export default function HookPage() {
  let [number, setNumber] = useState(1);
  //   [state, setState] = useState(defaultValue)
  let handleIncrease = () => setNumber(number + 1);

  let handleDescrease = () => setNumber(number - 1);

  let handleReset = () => setNumber(0);
  return (
    <div className="text-center bg-dark text-white">
      <button onClick={handleDescrease} className="btn btn-danger">
        -
      </button>
      <strong> {number} </strong>
      <button onClick={handleIncrease} className="btn btn-warning">
        +
      </button>
      <Footer handleReset={handleReset} number={number} />
    </div>
  );
}
// trong class có gì thì trong function cũng có tương tự
