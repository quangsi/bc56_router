import React from "react";
import { TAI, XIU } from "./utils";
import { useDispatch, useSelector } from "react-redux";
import { CHOOSE_OPTION } from "../../redux/constant/xucXac";

export default function XucXac() {
  // useSelector ~ mapStateToProps
  // useDispatch ~ mapDispatchToProps
  let mangXucXac = useSelector((state) => state.xucXacReducer.mangXucXac);
  let dispatch = useDispatch();
  let handleLuaChon = (luaChon) => {
    let action = {
      type: CHOOSE_OPTION,
      payload: luaChon,
    };
    dispatch(action);
  };
  return (
    <div className="d-flex align-items-center justify-content-center">
      <button
        onClick={() => {
          handleLuaChon(TAI);
        }}
        className="p-5 btn btn-danger"
      >
        Tài
      </button>
      <div className="mx-5">
        {mangXucXac.map((item) => {
          return <img width={80} src={item.img} alt="" />;
        })}
      </div>
      <button
        onClick={() => {
          handleLuaChon(XIU);
        }}
        className="p-5 btn btn-dark"
      >
        Xỉu
      </button>
    </div>
  );
}
