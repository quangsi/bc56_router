import React, { useState } from "react";
import "./style.css";
// hình để trong src=> import
import bgGame from "./bg_game.png";
import XucXac from "./XucXac";
import KetQua from "./KetQua";
import { TAI, XIU, defaultXucXac } from "./utils";
import { message } from "antd";

export default function Ex_XucXac_Redux() {
  const [mangXucXac, setMangXucXac] = useState(defaultXucXac);
  const [luaChon, setLuaChon] = useState();
  const [soLuotChoi, setSoLuotChoi] = useState(0);
  const [soLanThang, setSoLanThang] = useState(0);

  let handlePlayGame = () => {
    let min = 1;
    let max = 6;
    let sum = 0;
    let newXucXac = mangXucXac.map((item) => {
      let number = Math.floor(Math.random() * (max - min + 1) + min);
      sum += number;
      return {
        img: `https://baitap-react-redux-gamexucxac.vercel.app/image_Game_XucXac/${number}.png`,
        value: number,
      };
    });
    if ((luaChon == XIU && sum < 11) || (luaChon == TAI && sum >= 11)) {
      setSoLanThang(soLanThang + 1);
      message.success(<span className="display-4">You Win</span>);
      // win
    } else {
      message.error(<span className="display-4">You Lose</span>);
    }
    setMangXucXac(newXucXac);
    setSoLuotChoi(soLuotChoi + 1);

    // tạo ra 3 con xúc xác mới với giá trị ngẫu nhiên từ 1 đến 6
    // random from to js
    // <11 => Xỉu
  };
  return (
    <div
      style={{
        backgroundImage: `url(${bgGame})`,
      }}
      className="game-container text-center pt-5"
    >
      <XucXac setLuaChon={setLuaChon} />
      <KetQua
        luaChon={luaChon}
        soLanThang={soLanThang}
        soLuotChoi={soLuotChoi}
        handlePlayGame={handlePlayGame}
      />
    </div>
  );
}
