import { defaultXucXac } from "../../page/Ex_XucXac_Redux/utils";
import { CHOOSE_OPTION, PLAY_GAME } from "../constant/xucXac";

const initialState = {
  mangXucXac: defaultXucXac,
  luaChon: null,
  soLuotChoi: 0,
  soLanThang: 0,
};

export let xucXacReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case PLAY_GAME: {
      let { mangXucXac } = state;
      let min = 1;
      let max = 6;
      let sum = 0;
      let newXucXac = mangXucXac.map((item) => {
        let number = Math.floor(Math.random() * (max - min + 1) + min);
        sum += number;
        return {
          img: `https://baitap-react-redux-gamexucxac.vercel.app/image_Game_XucXac/${number}.png`,
          value: number,
        };
      });
      return { ...state, mangXucXac: newXucXac };
    }
    case CHOOSE_OPTION: {
      return { ...state, luaChon: payload };
    }

    default:
      return state;
  }
};
