import { combineReducers } from "redux";
import { userReducer } from "./userReducer";
import { xucXacReducer } from "./xucXacReducer";

export let rootReducer = combineReducers({ userReducer, xucXacReducer });

// redux devtool
